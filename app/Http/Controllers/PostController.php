<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\ImageUploadTrait;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    use ImageUploadTrait;
   /**
     * created product page the form for creating a new resource.
     */
    public function search(Request $request)
    {
        $query = $request->input('q');

        $posts = Post::where('title', 'like', "%$query%")
                     ->orWhere('content', 'like', "%$query%")
                     ->get();

                     return view('post.index',compact('posts'));
    }
    public function create()
    {
        $user = Auth::user();

        // Check if the user has role 1 (admin)
        if ($user->role == 1) {
            // User has admin role, fetch all posts
            $posts = Post::orderBy('id', 'desc')->paginate(2);
        } else {
            // User has regular role, fetch only their own posts
            $posts = Post::where('user_id', $user->id)->paginate(2);
        }
    
        return view('post.index', compact('posts'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function storeOrUpdate(Request $request)
    {
        // dd($request->all());
        $validatedData = $request->validate([
            'user_id' => 'exists:users,id',
            'title' => 'required|string|max:255',
            'content' => 'required|string',
            'photo' => 'nullable|file',
            'published_at' => 'nullable|date',
        ]);

        // Handle image upload
        if ($request->hasFile('photo')) {
            $photo = $this->saveImage($request->file('photo'));
            $validatedData['photo'] = $photo;
        }

        // Get authenticated user's ID
        $authUserId = auth()->id();

        // Check if the request contains a post ID for updating
        $postId = $request->input('id');
        $post = Post::updateOrCreate(
            ['id' => $postId, 'user_id' => $authUserId],
            $validatedData
        );

        // Optionally, you can return a response or redirect
        return response()->json(['message' => 'Post created or updated successfully', 'post' => $post]);
     }
    
    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        // dd($id);
        $posts = Post::findOrFail($id);
        return response()->json(['status' => 'success', 'data' => $posts]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        try {
            $post = Post::findOrFail($id);

            $post->name = $request->name;
            $post->price = $request->price;
            $post->description = $request->description;

            $post->save();

            return response()->json($post);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to update post.'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
         $post = Post::find($id);
        if (!$post) {
            return response()->json(['message' => 'post not found.'], 404);
        }
        $post->delete();
        return response()->json(['message' => 'post deleted successfully.']);
    }
}
