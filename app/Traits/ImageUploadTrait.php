<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait ImageUploadTrait
{
    public function saveImage($imageData)
    {
        $imageName = Str::uuid() . '.' . $imageData->getClientOriginalExtension();
        $imagePath = $imageData->storeAs('images', $imageName, 'public');

        return $imagePath;
    }

}
