@extends('layout.master')
@section('style')
<style>
    .pagination .page-link {
        font-size: 16px !important; /* Adjust the size as needed */
    }

</style>
@endsection
@section('content')
    <div class="container">
        <header>
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <!-- Navbar brand -->
                   
                    <span> {{ Auth::user()->name }}</span>
                    <a href="{{route('logout')}}"> Logout</a>
    
                    <!-- Toggle button for mobile view -->
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
    
                    <!-- Navbar items -->
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <!-- Add any additional navbar items here -->
                        </ul>
                       
                    </div>
                </div>
            </nav>
        </header>
        <h1>Post Add</h1>
        <form id="search">
            <input type="text" name="q" placeholder="Search...">
            <button type="submit">Search</button>
        </form>
        <div class="row">
            <div class="col-md-4 card mr-2">
                <form id="postAddForm" class="p-5">
                    @csrf
                    <!-- Hidden input field for product_id -->
                    <input type="hidden" name="id" id="post_id" value="">
                    <div class="mb-4 row">
                        <label for="productName" class="">Post Name</label>
                        <input type="text" name="title" class="form-control" id="title"
                            placeholder="Enter post name">
                    </div>
                    <div class="mb-4 row">
                        <label for="postContent" class="">Post Content</label>

                        <input name="content" type="text" class="form-control" id="content"
                            placeholder="Enter post content">
                    </div>
                    <div class="mb-4 row">
                        <label for="postContent" class="">Image</label>
                        <input name="photo" class="form-control form-control-sm"
                        id="photo" type="file" />
                    </div>
                    <div class="mb-4 row">
                        <label for="productDescription" class="">Published At</label>
                        <input type="date" name="published_at" class="form-control" id="published_at"
                            placeholder="Enter product description">

                    </div>
                    <div class="mb-4 row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary px-5 float-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            {{-- <div class="col-md-1"></div> --}}
            <div id="searchResults"></div>
            <div class="col-md-7 card">
                <div class="___table p-2">
                    <div class="table-responsive scrollbar">
                        <table class="table table-bordered">
                            <thead class="table-active">
                                <tr>
                                    <th scope="col">Title</th>
                                    <th scope="col">Content</th>
                                    <th scope="col">Published</th>
                                    <th class="text-end" scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="postList">
                                @foreach ($posts as $row)
                                    <tr>
                                        <td>{{ $row->title }}</td>
                                        <td>{{ $row->content }}</td>
                                        <td>{{ $row->published_at }}</td>
                                        <td class="text-end">
                                            <button class="btn btn-primary edit-post"
                                                data-post-id="{{ $row->id }}">Edit</button>
                                            <button class="btn btn-danger delete-post"
                                                data-post-id="{{ $row->id }}">Delete</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $posts->links() }} <!-- Pagination links -->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            // Event listener for edit-product button click
            $('.edit-post').click(function() {
                var postId = $(this).data('post-id');
                // alert(productId);
                // Fetch product data using AJAX
                $.ajax({
                    type: 'get',
                    url: "{{ url('post/edit') }}" + '/' + postId,
                    // data: formData,
                    success: function(response) {
                        // Handle success response
                        console.log(response.data);
                        $('#post_id').val(response.data.id);
                        $('#title').val(response.data.title);
                        $('#content').val(response.data.content);
                        $('#published_at').val(response.data.published_at);
                        $('#photo').val(response.data.photo);
                        // alert(response.message);
                    },
                    error: function(xhr, status, error) {
                        // Handle error response
                        console.error(xhr.responseText);
                        alert('Failed to save post.');
                    }
                });
                // Fill the form fields with product data

            });

            // Listen for the form submission
            $('#postAddForm').submit(function(event) {
                event.preventDefault();
                // Serialize form data
                var formData = $(this).serialize();
                // Perform AJAX call
                $.ajax({
                    type: 'POST',
                    url: "{{ url('post/storeOrUpdate') }}",
                    data: formData,
                    success: function(response) {
                        // Handle success response
                        alert(response.post);
                        // Redirect to the product add page
                     window.location.href = '/post/add';
                    },
                    error: function(xhr, status, error) {
                        // Handle error response
                        console.error(xhr.responseText);
                        alert('Failed to save post.');
                    }
                });
            });
            $('#search').submit(function(event) {
    event.preventDefault();
    // Serialize form data
    var formData = $(this).serialize();
    // Perform AJAX call
    $.ajax({
        type: 'GET',
        url: "{{ url('search') }}", // assuming the search route is '/search'
        data: formData,
        success: function(response) {
            // Handle success response
            displaySearchResults(response);
        },
        error: function(xhr, status, error) {
            // Handle error response
            console.error(xhr.responseText);
            alert('Failed to search posts.');
        }
    });
});
function displaySearchResults(posts) {
    // Assuming you have a div with id 'searchResults' to display the search results
    $('#searchResults').empty(); // Clear previous results
    $.each(posts, function(index, post) {
        $('#searchResults').append('<div><h2>' + post.title + '</h2><p>' + post.content + '</p></div>');
    });
}

            // Event listener for delete-product button click
            $('.delete-post').click(function(event) {
                event.preventDefault();
                var postId = $(this).data('post-id');
                // Send AJAX request to delete the product
                $.ajax({
                    type: 'DELETE',
                    url: "{{ url('post/delete') }}/" + postId,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        // Handle success response
                        alert('post deleted successfully.');
                        // Reload the page to update the product list
                        location.reload();
                    },
                    error: function(xhr, status, error) {
                        // Handle error response
                        console.error(xhr.responseText);
                        alert('Failed to delete post.');
                    }
                });
            });
        });
    </script>
@endsection
