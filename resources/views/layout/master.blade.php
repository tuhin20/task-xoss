<!DOCTYPE html>
<html lang="en">
@include('layout.style')
@yield('style')
<body>
    @yield('content')
    <!-- jQuery -->
    @include('layout.js')
    @yield('js')
    <!-- Your custom JavaScript code --> 
</body>
</html>
